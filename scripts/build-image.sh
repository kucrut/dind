#!/bin/sh

set -e

if [ -z ${1+x} ]; then
    echo 'ERROR: Missing variant.'
    exit 1
fi

VARIANT="${1}"
DOCKERFILE="variants/${VARIANT}/Dockerfile"
IMAGE_NAME="${CI_PROJECT_PATH}:${VARIANT}"

echo $DOCKER_PASSWORD | docker login -u $DOCKER_USER --password-stdin

docker pull $IMAGE_NAME || true
docker build --cache-from $IMAGE_NAME -f $DOCKERFILE -t $IMAGE_NAME .

if [ $CI_COMMIT_BRANCH = $CI_DEFAULT_BRANCH ]; then
    docker push $IMAGE_NAME
fi
